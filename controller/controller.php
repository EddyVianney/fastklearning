<?php
require_once('../../model/ItemManager.php');
require_once('../../model/panier.php');
require_once('../../model/commentManager.php');

function listComments()
{
	$commentManager = new commentManager;
	$ItemManager    = new ItemManager();
	$item =  $ItemManager->getItem($_GET['id']);
	$comments = $commentManager->getComments($_GET['id']);
	$nbrComment = $comments->rowCount();
	$titre = $item['titre'];
	$description = $item['descpt'];
	$price = number_format((float)$item['prix'],2,'.','');
	$photo = '../../public/images/'.$item['photo'];
	
	echo '
			 <div class="category">
            <span>Description</span>
        	</div>
			<div class="it-view media">
                <div class="media-left">
                    <img  class="view-img-item" src="'.$photo.'" class="media-object" style="width:200px">
                </div>
                <div class="media-body">
                  <h4 class="it-view-title media-heading"><span>'.$titre.'</span></h4>
                  <p class="it-view-price">'.$price.' &#8364;</p>
                  <p class="ligne-dec"></p>
                  <p class="it-view-desc">'.$description.'</p>
                </div>
            </div>
          <div class="category">
            <span>Comments</span>
        </div>
        <p class="nbr-view-comment">'.$nbrComment.' comment(s)</p>';

    if($comments->rowCount())
	{
		while($comment = $comments->fetch())
		{
			
			if(empty($comment['userPhoto']))
			{
				$avatar = '../../public/images/avatar.jpg';
			}
			else
			{
				$avatar = '../../public/images/'.$comment['userPhoto'];
			}

			echo '
			<div class="media comment-box">
                <div class="media-left">
                    <img src="'.$avatar.'" class="media-object" style="width:60px">
                </div>
                <div class="media-body">
                  <h4 class="media-heading">'.$comment['pseudo'].' <span style="font-style:italic;font-size:14px;">posted on the '.date("jS F, Y", strtotime($comment['dateComment'])).'</span></h4>
                  <p>'.$comment['comment'].'</p>
                </div>
              </div>';
		}
	}

}
function  displayItem()
{
	$ItemManager = new ItemManager();
	$item = $ItemManager->getItem($_GET['id']);
	return $item ;
}

function listCartItems()
{
	 $panier = new Panier();
     $items = $panier->getItems();

    if(!empty($items) && $items->rowCount())
    {
    	while($item = $items->fetch() )
        {

            $photo = "../../public/images/".$item['photo'];
              echo '
            <tr>
              	<td class="td-image">
                	<a href="#" class="pnr-img"><img src="'.$photo.'"></a>
              	</td>
              	<td class="td-info">
                	<span class="pnr-img-info">
                    <span class="pnr-titre">'.$item['titre'].'</span>
                    <p class="pnr-descpt">'.$item['descpt'].'</p>
               	   </span>
                </td>
                <td class="pnr-price">'.$item['prix'].' &#8364;</td>  
                <td class="td-qty">
               		<div class="pnr-quantity">
                  		<div class="input-group">
                    		<input type="text" class="form-control" name="panier[qt]['.$item['id_item'].']" value="'.$panier->getQuantity($item['id_item']).'">
                      		<span class="input-group-addon"><i class="glyphicon glyphicon-shopping-cart"></i></span>
                  		</div>
              		</div>
            	</td>
            </tr>
              ';
            }
    }
}

function listItems()
{
	$ItemManager = new ItemManager();
	$items = $ItemManager->getItems();

	if($items->rowCount())
	{
      while($item = $items->fetch())
      {

         $photo = '../../public/images/'.$item['photo'];
        echo '
          <div class="col-md-4">
  			<div class="flip-box">
   				<div class="flip-box-inner">
    				<div class="flip-box-front">
       					<div class="thumbnail">    
              				<img src="'.$photo.'" alt="...">
              				<div class="caption">
                			<h4 class="titre">'.$item['titre'].'</h4>
             			</div>
       				</div>
    			</div>
    			<div class="flip-box-back">
       				<p class="titre">'.$item['titre'].'</p>
        			<p class="descpt">'.$item['descpt'].'</p>
        			<p class="prix">'.number_format((float)$item['prix'],2,'.','').' &#8364;</p>
          			<input type="hidden" name="id_item" value = '.$item['id_item'].'>
          			<a class="tt panier-item" href="../../controller/controllerPanier.php?id='.$item['id_item'].'">Add to Cart <span class="glyphicon glyphicon-shopping-cart"></span></a>
          			<a class="see-commentsf btn-cart" href="../../view/frontend/viewItem.php?id='.$item['id_item'].'">see comments ...</a>
    			</div>
  			</div>
  		</div>
	</div>
        ';
      }
    }
    else
    {
    	echo '<div class="no-data">Sorry, there are no items available !</div>';
    }
}

function dataMessage()
{
	$panier = new Panier();
    if( (!isset($_SESSION['panier'])) || ( isset($_SESSION['panier']) && $panier->items_number() < 1 ) )
    {
    	echo '<div class="no-data">Sorry, your shopping cart is empty !</div>';
    }
}

function total()
{
	$panier = new Panier();
    echo $panier->sum_items();
}
?>