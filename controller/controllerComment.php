<?php
session_start();
require_once('../model/commentManager.php');

	if(isset($_POST['comment']))
	{
		$comment_json = array("comment"=>"","success"=>false,"redirect"=>true);

		if(isset($_SESSION['user']))
		{
			$id_item = $_POST['id'];
			$id_ut 	  =   $_SESSION['user']['id_ut'];
			$nickname =   $_SESSION['user']['pseudo'];
			$comment = $_POST['comment'];
			
			$photo = '../../public/images/'.$_SESSION['user']['photo'];
			$commentManager = new commentManager ;
			$result = $commentManager->postComment($id_item, $id_ut, $comment);
			$dateComment = $result['date'];

			if($result['status'])
			{
				$comment_json['dataComment']['date']     =  $dateComment;
				$comment_json['dataComment']['nickname'] =  $nickname;
				$comment_json['dataComment']['photo']    =  $photo;
				$comment_json['dataComment']['comment']  =  $comment;
				$comment_json['success'] = true ;
				$comment_json['redirect'] = false;
			}
		}
		
	}
echo json_encode($comment_json);
?>
