<?php

require("../model/panier.php");

$panier = new Panier();

/*modify items quantuty here */
if(isset($_POST['panier']['qt']))
{
	$data = array();
	$panier->setQuantityAll($_POST['panier']['qt']);
	$data['success'] = true ;
	$data['number'] = $panier->items_number();
    $data['price'] = $panier->sum_items();
    echo json_encode($data);
    exit;

}


if(isset($_GET['id']))
{
	$panier->add_item($_GET['id']);
	$data = array();
	$data['success'] = true ;
	$data['number'] = $panier->items_number();
    $data['price'] = $panier->sum_items();
    echo json_encode($data);
}
else
{
	
	$data = array("success"=>false);
	$data['number'] = $panier->items_number();
	$data['price'] = $panier->sum_items();
    echo json_encode($data);
}


?>