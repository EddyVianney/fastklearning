<?php
session_start();
require_once("../model/FormValidation.php");
require_once("../model/utilisateurManager.php");


$form = new FormValidation ;
$utilisateurManager = new UtilisateurManager;


if(!empty($_POST))
{

	if(!empty($_POST['signup']))
	{
		
	  $result = $form->registerUser($_POST);

	 if($result['success'])
	 {

	 	$user = $utilisateurManager->map($result['user']);
	 	$utilisateurManager->create($user);
	 }

	}

	if(!empty($_POST['signin']))
	{
		$result = $form->connectUser($_POST);
		if($result['success'])
		{
			$user = $utilisateurManager->map($result['user']);
			$userDB = $utilisateurManager->user_exist($user);

			if(empty($userDB))
			{
				$result['success'] = false;
			}
			else
			{
				$_SESSION['user']['id_ut'] 	   = $userDB['id_ut'];
				$_SESSION['user']['pseudo']    = $userDB['pseudo'];
				$_SESSION['user']['email'] 	   = $userDB['email'];
				$_SESSION['user']['mot_de_passe'] = $userDB['mot_de_passe'];
				$_SESSION['user']['photo'] = '../../public/images/'.$userDB['photo'];
			}
		}		
	}

}
else
{
	$result['user'] = array();
	$result['errors'] = array();
	$result['success'] = false;

}


echo json_encode($result);

?>