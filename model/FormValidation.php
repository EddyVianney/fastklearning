<?php

class FormValidation
{

	private $inputs;
	private static $success = true;
	private static $errors = array();

	const NAME = "name";
	const NICKNAME ="nickname";
	const EMAIL ="email";
	const PASSWORD ="password";
	
	private function secureInput($input)
	{
		$input = trim($input);
		$input = stripslashes($input); 
		$input = htmlspecialchars($input);

		return $input;
	}

	public function getErrors()
	{
		return self::$errors;
	}
	private function setError($field, $message)
	{
		self::$errors[$field] = $message;
	}

	private function validateName($name)
	{
		if(empty($name))
		{
			throw new Exception ("please, enter a valid name");
		}
	}

	private function validateNickname($nickname)  
	{
		if(empty($nickname))
		{
			throw new Exception ("please, enter a valid pseudo !");
		}
	}

	private  function validateEmail($email)
	{
		if(!empty($email))
		{
			if(!filter_var($email,FILTER_VALIDATE_EMAIL))
			{
				throw new Exception("please, enter a valid email address !");
			}
		}
		else
		{
			throw new Exception("please, enter an email address !");
		}
	}


	private function validatePassword($password1,$password2){

		if(!empty($password1) && !empty($password2) )
		{
			if($password1 != $password2)
			{
				throw new Exception("passwords do not match !");
			}
		}
		else
		{
			throw new Exception("please, enter or confirm your password !");
		}
	}

	private function validatePassword2($password)
	{

		if(empty($password))
		{
			throw new Exception("please, enter your password !");
		}
	}


	public function registerUser($array)
	{
			$result = array();
			$result['user'] = array();
			$result['success'] = true ;

			try{

				$this->validateName($array['name']);
			}
			catch(Exception $e)
			{
				$this->setError("name",$e->getMessage());
			}
			$result['user']['name'] =  $this->secureInput($array['name']);


			try{

				$this->validateNickname($array['nickname']);
			}
			catch(Exception $e)
			{
				$this->setError("nickname",$e->getMessage());
			}
			$result['user']['nickname'] =  $this->secureInput($array['nickname']);

			try{

				$this->validateEmail($array['email']);
			}
			catch(Exception $e)
			{
				$this->setError("email",$e->getMessage());
			}
			$result['user']['email'] =  $this->secureInput($array['email']);

			try{

				$this->validatePassword($array['password'],$array['passwordConfirm']);
			}
			catch(Exception $e)
			{
				$this->setError("password",$e->getMessage());
			}
			$result['user']['password'] =  $this->secureInput($array['password']);

			$result['errors'] = self::$errors;

			if(count($result['errors']) > 0)
			{
				$result['success'] = false ;
			}

		return $result ;
	}



public function connectUser($array)
   {
         $result = array();
         $result['user'] = array();
         $result['success'] = true ;

         try{

            $this->validateEmail($array['email']);
         }
         catch(Exception $e)
         {
            $this->setError("email",$e->getMessage());
         }
         $result['user']['email'] =  $this->secureInput($array['email']);

         try{

            $this->validatePassword2($array['password']);
         }
         catch(Exception $e)
         {
            $this->setError("password",$e->getMessage());
         }
         $result['user']['password'] =  $this->secureInput($array['password']);

         $result['errors'] = self::$errors;

         if(count($result['errors']) > 0)
         {
            $result['success'] = false ;
            $result['input'] = $result['errors']['email'];      
          }

      return $result ;
   }
	

}
?>