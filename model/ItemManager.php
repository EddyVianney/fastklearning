<?php
require_once('database.php');

class ItemManager extends Database
{

	
	public function getItem($id)
	{
		$db = self::connect();
		$item_request = "SELECT * FROM items WHERE id_item = ?";
		$item  = $db->prepare($item_request);
		$item->execute(array($id));
		return $item->fetch();
	}
	
	public function getItemComment($id)
	{
		$db = self::connect();
		$request = "SELECT * FROM item_comment  WHERE id_item = ?";
		$comments = $db->prepare($request);
		$comments->execute(array($id));
		return $comments ;

	}
	
	public function getItems()
	{
		$db = self::connect();
		$items_request = "SELECT * FROM items ";
		$list_items = $db->query($items_request);

		return $list_items;
	}

}
?>