<?php

require_once('database.php');

class commentManager extends Database
{

	public function postComment($id_item,$id_ut,$comment)
	{
		$db = self::connect();
		$request = "INSERT INTO item_comment (id_item, id_ut, comment,dateComment) VALUES (?,?,?,?)";
		$statement = $db->prepare($request);
		$dateComment = date("Y-m-d H:i:s");   
		$status = $statement->execute(array($id_item, $id_ut, $comment, $dateComment));
		$result = array();
		$result['date'] = $dateComment;
		$result['status'] = $status ;
		return $result;

	}

	public function getComment ($id_comment)
	{
		$db = self::connect();
		$request = "SELECT * FROM  item_comment WHERE id_comment = ? ";
		$statement = $db->prepare($request);
		$statement->execute(array($id_comment));
		return $statement;
	}
	public function countComment()
	{
		$db = self::connect();
		$request = "SELECT COUNT(*) FROM items_comment";
		$count = $db->query($request);
		return $count ;
	}

	public function getComments($id_item)
	{
		$db = self::connect();

		$request = "SELECT items.photo, items.titre, item_comment.comment, item_comment.dateComment, utilisateurs.photo as userPhoto, utilisateurs.pseudo
		FROM item_comment 
		INNER JOIN items ON item_comment.id_item = items.id_item 
		INNER JOIN utilisateurs ON utilisateurs.id_ut = item_comment.id_ut WHERE item_comment.id_item = ?";

		$comments = $db->prepare($request);

		$tt = $comments->execute(array($id_item));

		return $comments ;
	}
	
}

?>
