<?php

 class Database
 {
	 
	private static $dbHost = "localhost";  
	private static $dbName = "ttmik"; 
	private static $dbUser = "root";      
	private static $dbUserPassword = "vesperal1991";
	private static $connection = null;
	
	protected static function connect()  
	{   
		try
		{
			self::$connection = new PDO("mysql:host=".self::$dbHost.";dbname=".self::$dbName,self::$dbUser,self::$dbUserPassword);
			
		}
		
		catch(PDOException $e)
		{
			
			die($e->getMessage());
		}
		
		return self::$connection;
		
	}
	
	
	protected static function disconnect()
	{
		
		self::$connection = null;
	}

	}

?>