<?php

require_once('database.php');

class Panier extends Database
{


	public function __construct()
	{
		if(!isset($_SESSION))
		{
			session_start();
		}
		else 
		{
			
			//$_SESSION['panier'] = array();

		}
	}


	public function items_number()
	{
		$items_total = 0;

		foreach ($_SESSION['panier'] as $item) {
	
			$items_total +=  $item;
		}

		return $items_total ;
	}

	public function getQuantity($id)
	{
		if(!empty($_SESSION['panier'][$id]))
		{
			return $_SESSION['panier'][$id] ;
		}
	}
	
	public function setQuantity($id,$qt)
	{
		$_SESSION['panier'][$id] = $qt;
	}

	public function setQuantityAll($array)
	{
		foreach ($array as $key =>$value) {
			
			if($array[$key] == 0)
			{
				$this->remove_item($key);
			}
			else
			{
				$this->setQuantity($key, $array[$key]);
			}
		}
	}

	public function remove_item($id)
	{
		unset($_SESSION['panier'][$id]);
	}

	public function add_item($id)
	{
		if(isset($_SESSION['panier'][$id]))
		{
			$_SESSION['panier'][$id] ++;
		}
		else
		{
			$_SESSION['panier'][$id]  = 1;
		}
	}

	public function getItems()
	{
		
		$ids = array_keys($_SESSION['panier']);

		if(empty($ids))
		{
		
			$items = array();
		}
		else
		{
			$db = self::connect();
			$request = 'SELECT * FROM items WHERE id_item IN  ('.implode(',', $ids).')';
			$items = $db->query($request);

		}
		return $items;
	}

	public function sum_items()
	{
		$sum_total = 0;

		$ids = array_keys($_SESSION['panier']);

		if(empty($ids))
		{
		
			$items = array();
		}
		else
		{
			$db = self::connect();
			$request = 'SELECT id_item , prix FROM items WHERE id_item IN  ('.implode(',', $ids).')';
			$items = $db->query($request);

		}

		if(!empty($items))
		{
			while($item = $items->fetch())
			{

				$sum_total += $_SESSION['panier'][$item['id_item']]*$item['prix'] ;
			}

		}

		return $sum_total;
	}


}
?>