<?php

class Utilisateur {

	private $id ;
	private $name;
	private $nickname;
	private $email;
	private $password;
	private $photo;

	public function getId()
	{
		return $this->id ;
	}

	public function setId($id)
	{
		$this->id = $id ;
	}
	public function getName()
	{
		return $this->name;
	}

	public function getNickname()
	{
		return $this->nickname;
	}

	public function getEmail()
	{
		return $this->email;
	}

	public function getPassword()
	{
		return $this->password;
	}

	public function getPhoto()
	{
		return $this->photo;
	}


	public function setPhoto($photo)
	{
		$this->photo = $photo;
	}

	public function setNickname($nickname)
	{
		$this->nickname = $nickname;
	}

	public function setPassword($password)
	{
		$this->password = $password ;
		//$this->password = password_hash($password,PASSWORD_BCRYPT);
	}

	public function setName($name)
	{
		$this->name = $name;
	}
	
	public function setEmail($email)
	{
		$this->email = $email;
	}
	

}
?>