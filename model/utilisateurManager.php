<?php
require('database.php');
require('utilisateur.php');

class UtilisateurManager extends Database
{


	public function map($array)
	{
		$user = new Utilisateur;
		$user->setId($array['id_ut']);
		$user->setName($array['name']);
		$user->setNickname($array['nickname']);
		$user->setEmail($array['email']);
		$user->setPassword($array['password']);
		$user->setPhoto($array['photo']);

		return  $user;
	}

	 public function user_exist(Utilisateur $user)
   {
      $db = self::connect();
      $request = "SELECT * FROM utilisateurs WHERE email = ? ";
      $statement = $db->prepare($request);
      $statement->execute(array($user->getEmail()));

      if($statement->rowCount())
      {
         $userDB = $statement->fetch(PDO::FETCH_ASSOC);
         
         if(password_verify($user->getPassword(), $userDB['mot_de_passe']))
         {
            return $userDB;
         }
      }

      return NULL;
   }

	public function create(Utilisateur $user)
	{
		$db = self::connect();
		$request = "INSERT INTO utilisateurs (nom, pseudo, email,mot_de_passe,photo) VALUES (?,?,?,?,?)";
		$statement = $db->prepare($request);
		$hash = password_hash($user->getPassword(),PASSWORD_BCRYPT);
		$statement->execute(array($user->getName(),$user->getNickname(),$user->getEmail(),$hash,$user->getPhoto()));
	}

	public function read(Utilisateur $user)
	{
		$db = self::connect();
		$request = "SELECT * FROM  utilisateurs WHERE id_ut = ?";
		$statement = $db->prepare($request);
		$statement->execute($user->getId());
		return $statement->fetch(PDO::FETCH_ASSOC);
	}

	public function update()
	{

	}

	public function delete(Utilisateur $user)
	{
		$db = self::connect();
		$request = "DELETE FROM  utilisateurs WHERE id_ut = ?";
		$statement = $db->prepare($request);
		$statement->execute($user->getId());

	}

}
?>