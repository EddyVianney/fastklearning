$(function(){

$("#element").introLoader();

$("#comment-form").submit(function(e){

         e.preventDefault();
         var url  = $(this).attr('action');
         var data = $("#comment-form").serialize();
         $.ajax({

            type: 'POST',
            url:  url,
            data : data,
            dataType : 'json',
            success: function(result){
               if(result.success)
               {
                  var comment =  '<div class="media comment-box">\
                  <div class="media-left">\
                  <img src="'+result.dataComment.photo+'" class="media-object" style="width:60px">\
                  </div>\
                <div class="media-body">\
                <h4 class="media-heading">'+result.dataComment.nickname+' <span style="font-style:italic;">'+result.dataComment.date+'</span></h4>\
               <p>'+result.dataComment.comment+'</p>\
               </div>\
               </div>';
                  $(comment).hide().appendTo("#list-comment").fadeIn(2000);
                  $("#comment-form")[0].reset();
               }
               else
               {  
                  if(result.redirect)
                  {
                     var answer = confirm("do you want to log in to comment ?");
                     if(answer)
                     {
                        location.href = 'signin.php';
                     }
                  }
                  else
                  {
                     alert("veuillez indiquez votre commentaire");
                  }
               }
            }

         });

      });

})