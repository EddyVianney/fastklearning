$(function(){
      

      $("input").focus(function(){

            var id = $(this).attr('id');

            $("#" + id + " + .message").fadeOut(500);
      });

      $("#register").submit(function(e){

      	e.preventDefault();
      	var postdata = $("#register").serialize();
         var url = '../../controller/controllerUser.php';

      	$.ajax({

      		type: 'POST',
      		url: url,
      		data : postdata,
      		dataType : 'json',
      		success: function(result){

      			if(result.success)
      			{
      				  $("#form-message").empty().append('<span class="text-success">you have been successfully registered !</span>');
      				  $("#myForm")[0].reset();
      			}
      			else
      			{  
                     $("#name + .message").html(result.errors.password).addClass("input-error");
                     $("#password + .message").html(result.errors.password).addClass("input-error");
                     $("#email + .message").html(result.errors.email).addClass("input-error");
                     $("#nickname + .message").addClass("input-error").html(result.errors.nickname);
      			}
      		}

      	});

      });

      $("#signin").submit(function(e){

         e.preventDefault();
         var postdata = $("#signin").serialize();
         var url = '../../controller/controllerUser.php';

         $.ajax({

            type: 'POST',
            url: url,
            data : postdata,
            dataType : 'json',
            success: function(result){

               if(result.success)
               {
                  location.href = "index.php";
               }
               else
               {  
                     alert(result.input);
                     $("#password + .message").html(result.errors.password).addClass("input-error");
                     $("#email + .message").html(result.errors.email).addClass("input-error");
               }
            }

         });

      });


})