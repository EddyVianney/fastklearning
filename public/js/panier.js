$(function(){


   $(".see-comments").click(function(e){

   e.preventDefault();

      var url = $(this).attr('href');

     $.ajax({

            type: 'GET',
            url:  url,
            data : {},
            dataType : 'json',
            success: function(result){
               if(result.success)
               {
                     
                  $("#allComments").empty();

                  var pathImage = '../../public/images/avatar.jpg';
                  var titre = result.comments[0].titre;
                  var nbrComment = result.comments.length - 1 ;
                  var i;
                  $("#allComments").empty().append('<p class="text-center titre-view-comment">'+titre+'</p>');
                  $("#allComments").append('<p class="nbr-view-comment">'+nbrComment+' comment(s)</p>');
                  for(i = 0; i < result.comments.length ; i++)
                  {
                     $("#allComments").append('<div class="media comment-box">\
                     <div class="media-left">\
                        <img src='+pathImage+' class="media-object" style="width:60px">\
                     </div>\
                  <div class="media-body">\
                  <h4 class="media-heading">John Doe <span style="font-style:italic;">'+ result.comments[i].dateComment+'</span></h4>\
                  <p>'+ result.comments[i].comment +'</p>\
                   </div>\
                  </div>');
                  }
                   $("#allComments").append('<form id="comment-form"  method="post" action="../../controller/controllerComment.php">\
                  <div class="form-group">\
                   <textarea rows="6" class="form-control" name="comment" placeholder="what do you think of that book ?"></textarea>\
                  </div>\
                  <button  type="submit" class="btn-view-comment btn-cart">add comment</button>\
                 </form>');
                  $("#show-comments").fadeIn(2000);
               }   
               else
               {  
                  $("#allComments").html('<p class="text-center no-comment">there are no comments</p>');
                  $("#show-comments").fadeIn(2000);
               }
            }

         });
         $("#allComments").append('<script src="../../public/js/comment.js" type="text/javascript"></script>');


});



$(".panier-item").click(function(e){

         e.preventDefault();
         var url = $(this).attr('href');
         $.ajax({

            type: 'GET',
            url:  url,
            data : {},
            dataType : 'json',
            success: function(result){
               if(result.success)
               {
                 $("#snackbar").css("visibility", "visible");
                  $("#snackbar").animate({
                     bottom: '30px',
                     opacity: '1'
                  },200);
                  setTimeout(function(){
                  $("#snackbar").animate({
                     bottom: '0',
                     opacity: '0'
                  },200);
                  },1000);
               }   
               else
               {  
                  alert("echec de l'ajout");
               }
            }

         });

      });

  $("#mdqty").submit(function(e){

         e.preventDefault();
         
         var answer = confirm("do you really want to modify your command ?");

         if(answer)
         {

         var postdata = $("#mdqty").serialize();
         var url = $(this).attr('action');

         $.ajax({

            type: 'POST',
            url: url,
            data : postdata,
            dataType : 'json',
            success: function(result){

               if(result.success)
               {
                  $("#cart-total-price").html('Total '+ result.price +'&#8364;');
               }
               else
               {  
                  alert("sorry, an accident has occured during processing your request");
               }
            }

         });
         }
         else
         {
            alert("item quantity has not been modified");
         }
         
      });

      

      
})