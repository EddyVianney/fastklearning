<?php
session_start();
require_once('../../controller/controller.php');
?>
<!DOCTYPE html>
<html lang="en">
<?php require('header.php'); ?>
<body>
<div id="element" class="introLoading"></div>
<div id="snackbar">item successfully added</div>
<div id="show-comments" >
  <div id="allComments" class="container comments-content">
  </div>
</div>
<div class="container-fluid">
	<div class="row">
   <?php require_once('navbar.php'); ?>
    <div class="row">
      <div class="col-sm-offset-1 col-md-10">
        <?php listItems();?>
      </div>
    </div>
	</div>
</div>
<?php require('footer.php'); ?>
<script src="../../public/js/comment.js" type="text/javascript"></script>
<script src="../../public/js/panier.js" type="text/javascript"></script>
</body>
</html>