<?php
session_start();
?>
<nav class="navbar navbar-light">
      <div class="nav-container container-fluid" data-spy="affix" data-offset-top="1">
        <div class="navbar-header">
          <a class="navbar-brand dform-title" href="../../view/frontend/index.php">
            <img class="img-responsive" src="../../public/images/logo5.png">
          </a>
        </div>
        <ul class="up-menu nav navbar-nav navbar-right">
          <?php 
            if(isset($_SESSION['user']))
            {
              echo '
                <li><a href="#"" >'.$_SESSION['user']['pseudo'].' <span style="margin-left:5px;" class="glyphicon glyphicon-user"></span></a></li>
                <li><a href="logout.php" >log out <span class="glyphicon glyphicon-off"></span></a></li>
              ';
            }
            else
            {
               echo '
                    <li><a href="signup.php" ><span class="glyphicon glyphicon-user"></span> sign up</a></li>
                    <li><a href="signin.php" ><span class="glyphicon glyphicon-log-in"></span> sign in</a></li>
               ';
            }
          ?>
          <li><a href="viewPanier.php" id="notif" class="notification"><span class="glyphicon  glyphicon-shopping-cart"></span><span class="dnb-badge"></span></a></li>
        </ul>
        <ul class="down-menu nav navbar-nav navbar-right">
          <li><a href="#" id="learning">Learning Center</a></li>
          <li><a href="#" id="curriculum">Curriculum</a></li>
          <li><a href="#" id="store">Store</a></li>
        </ul>
      </div>
</nav>