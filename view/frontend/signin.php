<!DOCTYPE html>
<html lang="en">
<?php require('header.php'); ?>
<body>
<div class="container">
	<div class="row form-container">
		<div class="col-sm-offset-1 col-sm-10">
			<div class="title-form-image">
				<img class="img-responsive" src="../../public/images/logo5.png">
			</div>
			<form  id="signin" method="post" action="test.php" role="form">
				<input type="hidden"  name="signin" value="yes">
				<div class="row">
					<div class="form-group">
						<div class="row">
							<div class="col-sm-12">
								<label class="control-label" for="email">Your email</label>
								<input  type="text" id="email" name="email" class="form-control" placeholder="enter your email ...">
								<p class="message"></p>
							</div>
							<div class="col-sm-12">
								<label class="control-label" for="password">Your password</label>
								<input  id="password" type="password" name="password" class="form-control" placeholder="enter your password ...">
								<p class="message"></p>
							</div>
						</div>
					</div>
					<div  class="space col-sm-12">
						<div class="row">
						<button type="submit" id="submit" class="submit-btn btn">Sign in</button>
						</div>
					</div>
				</div>
				<div id="form-message" class="text-center" style="margin-top: 20px;"></div>
			</form>
		</div>
	</div>
</div>
<script src="../../public/js/form.js" type="text/javascript"></script>
</body>
</html>