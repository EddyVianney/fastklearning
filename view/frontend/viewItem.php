<?php
require_once('../../controller/controller.php');
?>
<!DOCTYPE html>
<html lang="en">
<?php require('header.php'); ?>
<body>
<div id="element" class="introLoading"></div>
<div class="container-fluid">
	<div class="row">
   <?php require('navbar.php'); ?>
    <div class="row">
      <div class="col-sm-offset-1 col-md-10 item-containero">
        <div id ="allComments" class="container comments-contentp col-sm-offset-2 col-sm-8">
          <div id="list-comment">
            <?php listComments(); ?>
          </div>
          <form id="comment-form" method="POST" action="../../controller/controllerComment.php">
            <input type="hidden"  name="id" value="<?php echo $_GET['id'];?>">
            <textarea rows="4" class="form-control" name="comment" placeholder="what do you think of that book ?"></textarea>
            <button  type="submit" class="btn-view-comment btn-cart">add comment</button>
          </form>
        </div>
        </div>
      </div>
    </div>
	</div>
<?php require('footer.php');?>
<script src="../../public/js/comment.js" type="text/javascript"></script>
<script src="../../public/js/panier.js" type="text/javascript"></script>
</body>
</html>