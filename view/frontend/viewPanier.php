<?php
  require_once('../../controller/controller.php');
?>
<!DOCTYPE html>
<html lang="en">
<?php require('header.php');?>
<body>
<div class="container-fluid">
  <div class="row">
    <?php require('navbar.php');?>
    <div class="row">
      <div class="col-sm-offset-1 col-md-10">
        <form id="mdqty" method="POST" action="../../controller/controllerPanier.php">
          <table class="table">
        <thead>
          <tr>
            <th>Products</th>
            <th>Description</th>
            <th>Price</th>
            <th>Quantity</th>
          </tr>
        </thead>
        <tbody>
         <?php listCartItems();?>
        </tbody>
      </table>
      <?php dataMessage();?>
      <button type="submit" class="btn-wdth submit-btn">update your shopping cart</button>
    </form>
     <div class="page-header total">
         <div id="cart-total-price">Total <?php total(); ?> &#8364;</div>
         <button class="btn-cart">commander <span class="glyphicon glyphicon-shopping-cart"></span></button>
    </div>
    <div id="snackbar">some text </div>
    </div>
    </div>
  </div>
</div>
<?php require('footer.php'); ?>
<script src="../../public/js/panier.js" type="text/javascript"></script>
<script src="../../public/js/comment.js" type="text/javascript"></script>
</body>
</html>